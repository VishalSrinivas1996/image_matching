import cv2.cv2 as cv2
import numpy as np
from keras import Model
from keras.models import load_model
from keras.applications import VGG19
from keras.applications import ResNet50
from keras.applications.imagenet_utils import preprocess_input
import keras.backend as K


def contrastive_loss(y_true, y_pred):
    margin = 1
    return K.mean(y_true * K.square(y_pred) + (1 - y_true) * K.square(K.maximum(margin - y_pred, 0)))


# siamese = load_model('./Slope/models/pill_siamese_best.h5', custom_objects={'contrastive_loss': contrastive_loss})
siamese = load_model('./Slope/models/pill_siamese_best.h5', compile=False)
base_model = VGG19(weights='imagenet')
model = Model(input=base_model.input,
              output=base_model.get_layer('block5_pool').output)
Resnet = ResNet50(weights='imagenet', include_top=False)


def pre_process_feature(img):
    img = np.expand_dims(img, axis=0)
    img = img.astype('float64')
    img = preprocess_input(img)
    features_VGG19 = model.predict(img).flatten()
    features_resnet = Resnet.predict(img).flatten()
    features = np.concatenate((features_VGG19, features_resnet))
    features = features.reshape(1, -1)
    return features


# image_sample = '/home/ml/PycharmProjects/ImageMatching/Slope/Negative_Pills_2/1.bmp'
# image_to_pred = '/home/ml/PycharmProjects/ImageMatching/Slope/Negative_Pills_2/2.bmp'

image_sample = '/home/ml/PycharmProjects/ImageMatching/Slope/Positive_Pills_2/5.bmp'
image_to_pred = '/home/ml/PycharmProjects/ImageMatching/Slope/Positive_Pills_3/1.bmp'


img_sample = cv2.imread(image_sample)
img_real = cv2.imread(image_to_pred)

cv2.imshow('Base', img_sample)
cv2.imshow('Test', img_real)

features_sample = pre_process_feature(img_sample)
np.save('sample_feature_slope_crocin_VRESNET_5bmp', features_sample)
features_real = pre_process_feature(img_real)

pred = siamese.predict([features_sample, features_real])
print(pred)
cv2.waitKey(0)