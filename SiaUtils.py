import numpy.random as rng
import numpy as np
import random 
from sklearn.model_selection import train_test_split


class SimNet_Utilies:

    def __init__(self, pill_train, pill_val):
        self.pill_data = pill_val
        self.pill_label = pill_train

    def load_data(self):
        x_train, x_test, y_train, y_test = train_test_split(self.pill_data, self.pill_label, test_size=0.33, random_state=42)
        return (x_train, y_train), (x_test, y_test)
    
    def pair_generator(self, numbers):
        used_pairs = set()

        while True:
            pair = random.sample(numbers, 2)
            pair = tuple(sorted(pair))
            if pair not in used_pairs:
                used_pairs.add(pair)
                yield pair

    def create_pairs(self, samples):
        positive_pairs = []
        negative_pairs = []
        labels_indices = self.pill_label.tolist()
        positive_indices = [i for i, x in enumerate(labels_indices) if x == 1]
        negative_indices = [i for i, x in enumerate(labels_indices) if x == 0]
        positive_gen = self.pair_generator(positive_indices)
        negative_gen = self.pair_generator(negative_indices)
        for i in range(samples):
            pair = positive_gen.__next__()
            positive_pairs.append(np.vstack((self.pill_data[pair[0]], self.pill_data[pair[1]])))

        for j in range(samples):
            pair = negative_gen.__next__()
            negative_pairs.append(np.vstack((self.pill_data[pair[0]], self.pill_data[pair[1]])))

        positive = np.array(positive_pairs)
        negative = np.array(negative_pairs)
        positive_labels = np.ones(positive.shape[0])
        negative_labels = np.zeros(negative.shape[0])
        pill_pair_label = np.hstack((positive_labels, negative_labels))
        pill_pair_data = np.vstack((positive, negative))
        print(pill_pair_data.shape, pill_pair_label.shape)
        return pill_pair_data, pill_pair_label

    def compute_accuracy(self, predictions, labels):
        return labels[predictions.ravel() < 0.5].mean()
