import os

import cv2
import numpy as np

path = '/home/ml/PycharmProjects/ImageMatching/Slope/Advance_2/'

print("\nLoading pre-trained model...\n")

print("\nReading images from '{}' directory...\n".format(path))

final_images = []

for subdir, dirs, files in os.walk(path):
    for filename in files:
        if filename.endswith(".bmp"):
            filename_full = os.path.join(subdir, filename)
            img = cv2.imread(filename_full)
            final_images.append(img)

final_images = np.array(final_images)
print(final_images.shape)

np.save('/home/ml/PycharmProjects/ImageMatching/Slope/npys/Advance_images', final_images)
