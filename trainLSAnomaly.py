import numpy as np
from lsanomaly import LSAnomaly
import argparse
from sklearn.externals import joblib
import os

ap = argparse.ArgumentParser()

ap.add_argument("-p", "--feature", required=True,
                help="path to the train images")

ap.add_argument("-m", "--model", default="0",
                help="model name with path")

args = vars(ap.parse_args())
feature_path = args["feature"]
model_path = args["model"]
lsanomaly = LSAnomaly(sigma=200.5813622285709, rho=12.749851717337275, n_kernels_max=1000)
features = np.load(feature_path)
print(features.shape)
lsanomaly.fit(features)

joblib.dump(lsanomaly, model_path)