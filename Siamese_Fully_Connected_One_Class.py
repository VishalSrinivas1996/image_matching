from __future__ import absolute_import
from __future__ import print_function

import numpy as np
from sklearn.model_selection import train_test_split

np.random.seed(1337)

import random
from keras.models import Sequential, Model
from keras.layers import Dense, Dropout, Input, Lambda
from keras.optimizers import Adam
from keras import backend as K
from keras.callbacks import ModelCheckpoint



def load_data():
    pill_data = np.load('./Slope/npys/pill_data_2.npy')
    pill_label = np.load('./Slope/npys/pill_label_2.npy')
    x_train, x_test, y_train, y_test = train_test_split(pill_data, pill_label, test_size=0.33, random_state=42)
    return (x_train, y_train), (x_test, y_test)


def euclidean_distance(vects):
    x, y = vects
    return K.sqrt(K.sum(K.square(x - y), axis=1, keepdims=True) + 0.000001)


def distance_output_shape(shapes):
    shape1, shape2 = shapes
    return shape1[0], 1


def contrastive_loss(y_true, y_pred):
    # margin = 10
    return K.mean(y_true * (y_pred*y_pred*y_pred))


def pair_generator_positive(positive_indices):
    used_pairs = set()
    while True:
        pair = random.sample(positive_indices, 2)
        pair = tuple(sorted(pair))
        if pair not in used_pairs:
            used_pairs.add(pair)
            yield pair

def create_pairs(pill_data, pill_labels, samples):
    positive_pairs = []
    labels_indices = pill_labels.tolist()
    positive_indices = [i for i, x in enumerate(labels_indices) if x == 1]
    positive_gen = pair_generator_positive(positive_indices)
    
    for i in range(samples):
        pair = positive_gen.__next__()
        positive_pairs.append(np.vstack((pill_data[pair[0]], pill_data[pair[1]])))

    positive = np.array(positive_pairs)
    positive_labels = np.ones(positive.shape[0])
    idx = np.random.permutation(len(positive_labels))
    pill_pair_data_shuffled, pill_pair_label_shuffled = positive[idx], positive_labels[idx]
    print(pill_pair_data_shuffled.shape, pill_pair_label_shuffled.shape)
    return pill_pair_data_shuffled, pill_pair_label_shuffled

def create_base_network(input_dim):
    model = Sequential()
    model.add(Dense(1024, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(512, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(128, activation='relu'))
    return model


def compute_accuracy(predictions, labels):
    return labels[predictions.ravel() < 1].mean()


(X_train, y_train), (X_test, y_test) = load_data()
X_train = X_train.astype('float32')
X_test = X_test.astype('float32')
input_dim = 25088
nb_epoch = 10

tr_pairs, tr_y = create_pairs(X_train, y_train, 5000)
te_pairs, te_y = create_pairs(X_test, y_test, 1000)

base_network = create_base_network(input_dim)

input_a = Input(shape=(input_dim,))
input_b = Input(shape=(input_dim,))

processed_a = base_network(input_a)
processed_b = base_network(input_b)

distance = Lambda(euclidean_distance, output_shape=distance_output_shape)([processed_a, processed_b])

model = Model(input=[input_a, input_b], output=distance)

adam = Adam()

filepath="crocin_one_class_best_10_cube.h5"

checkpoint = ModelCheckpoint(filepath, monitor='val_loss', verbose=1, save_best_only=True, mode='min')
callbacks_list = [checkpoint]

model.compile(loss=contrastive_loss, optimizer=adam)
model.fit([tr_pairs[:, 0], tr_pairs[:, 1]], tr_y,
          validation_data=([te_pairs[:, 0], te_pairs[:, 1]], te_y),
          batch_size=32,                                                                                                                                                                                                                                                                                
          nb_epoch=nb_epoch,
          callbacks=callbacks_list, 
          verbose=1)

model.save('crocin_one_class_dandanakka_1.h5')

pred = model.predict([tr_pairs[:, 0], tr_pairs[:, 1]])
tr_acc = compute_accuracy(pred, tr_y)
pred = model.predict([te_pairs[:, 0], te_pairs[:, 1]])
te_acc = compute_accuracy(pred, te_y)                                                                                                                                                                                                                                                                                                                                                                                                                                           

print('Accuracy on training set: %0.2f%%' % (100 * tr_acc))
print('Accuracy on test set: %0.2f%%' % (100 * te_acc))
